var express =   require("express");
var multer  =   require('multer');
var app         =   express();
app.get('/',function(req,res){
      res.sendFile(__dirname + "/index.html");
});


var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, __dirname+'/upload');
  },
  filename: function (req, file, callback) {
      console.log(file);
    callback(null, file.originalname);
  }
});

app.post('/file_upload',function(req,res){
    var upload = multer({ storage : storage}).single('file');
    upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");    
        }
        console.log(req.file.filename);
        res.end("File is uploaded");
    });
});

app.listen(8081,function(){
    console.log("Working on port 8081");
});